/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-21T10:45:34+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <string>

#include <NTOFLogging.hpp>

#include "BCTReader.h"
#include "Config.h"

using namespace ntof::bct;

int main(int argc, char **argv)
{
    ntof::log::init(argc, argv);
    const std::string configFile = (argc > 1) ? std::string(argv[1]) :
                                                Config::configFile;

    try
    {
        // Load configuration
        Config::load(configFile, Config::configMiscFile);
        Config &conf = Config::instance();

        const std::string &dimDNS = conf.getDimDns();
        DimClient::setDnsNode(dimDNS.c_str());
        DimServer::setDnsNode(dimDNS.c_str());

        BCTReader reader(conf.getDestination(), conf.getEvent(),
                         conf.getSource());
        DimServer::start(conf.getServer().c_str());
        reader.run();
    }
    catch (const ntof::NTOFException &ex)
    {
        std::cerr << "Caught n_TOF exception : " << ex.what() << std::endl;
        std::cerr << "Exiting..." << std::endl;
        return -1;
    }
    return 0;
}
