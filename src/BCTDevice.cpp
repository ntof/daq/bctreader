/*
 * BCTDevice.cpp
 *
 *  Created on: May 13, 2015
 *      Author: mdonze
 */

#include "BCTDevice.h"

#include <iostream>

#include <Timestamp.h>
#include <easylogging++.h>

#include "Config.h"

namespace ntof {
namespace bct {

utils::Timestamp TimedBuffer::retrieveTimestamp(const dim::DIMData::List &event)
{
    try
    {
        const dim::DIMData &data = event.at(
            Config::instance().getCycleStampIndex());
        return utils::Timestamp(data.getLongValue());
    }
    catch (...)
    {
        LOG(ERROR) << "[TimedBuffer]: failed to get cycleStamp";
        return utils::Timestamp(-1);
    }
}

BCTDevice::BCTDevice(const std::string &serviceName, int64_t timeMarginUsec) :
    m_client(serviceName)
{
    m_client.setHandler(this);
    m_timedBuffer.setTimeMargin(timeMarginUsec);
}

/**
 * Called when the DIM service is refreshed by the server
 * @param dataSet New list of data
 * @param client Client responsible of this callback (can be shared)
 */
void BCTDevice::dataChanged(dim::DIMData::List &dataSet,
                            const ntof::dim::DIMProxyClient &)
{
    m_timedBuffer.addEvent(dataSet);
}

/**
 * Called when an error occured on client (NO-LINK, NOT READY...)
 * @param errMsg Error message
 * @param client Client responsible for this callback (can be shared)
 */
void BCTDevice::errorReceived(const std::string &errMsg,
                              const ntof::dim::DIMProxyClient &)
{
    LOG(ERROR) << "Got error while reading BCT " << errMsg;
}

dim::DIMData::List BCTDevice::waitAndGetBCTData(const utils::Timestamp &ts,
                                                int32_t timeoutMsec)
{
    dim::DIMData::List event;
    m_timedBuffer.getEventByTime(event, ts, timeoutMsec);
    return event;
}

dim::DIMData::List BCTDevice::getLatestBCTData()
{
    return m_client.getLatestData();
}

} /* namespace bct */
} /* namespace ntof */
