//
// Created by matteof on 2020-06-15.
//

#ifndef CONFIG_H
#define CONFIG_H

#include <ConfigMisc.h>
#include <Singleton.hpp>
#include <pugixml.hpp>

namespace ntof {
namespace bct {

class Config :
    public ntof::utils::ConfigMisc,
    public ntof::utils::Singleton<Config>
{
public:
    /*
     * \brief Destructor of the class
     */
    virtual ~Config() = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     *
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file = configFile,
                        const std::string &miscFile = configMiscFile);

    const std::string &getSource() const;
    uint32_t getCycleStampIndex() const;
    const std::string &getEvent() const;
    const std::string &getDestination() const;
    const std::string &getServer() const;

    static const std::string configFile;     //!< Path of the config file
    static const std::string configMiscFile; //!< Path of the global config file

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile,
                    const std::string &miscFile = configMiscFile);

    std::string m_source;        //!< CASTOR svc class
    uint32_t m_cyclestamp_index; //!< DIM service history count
    std::string m_event;         //!< CASTOR svc class
    std::string m_destination;   //!< CASTOR svc class
    std::string m_server;        //!< CASTOR svc class
};

} // namespace bct
} // namespace ntof

#endif // CONFIG_H
