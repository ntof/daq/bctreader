/*
 * BCTDevice.h
 *
 *  Created on: May 13, 2015
 *      Author: mdonze
 */

#ifndef BCTDEVICE_H_
#define BCTDEVICE_H_

#include <string>

#include <boost/thread.hpp>

#include <DIMDataTimedBuffer.hpp>
#include <DIMProxyClient.h>
#include <Timestamp.h>

namespace ntof {
namespace bct {

class TimedBuffer : public ntof::utils::DIMDataTimedBuffer
{
protected:
    utils::Timestamp retrieveTimestamp(const dim::DIMData::List &event) override;
};

/**
 * Class to read BCT device
 */
class BCTDevice : public ntof::dim::DIMProxyClientHandler
{
public:
    explicit BCTDevice(const std::string &serviceName, int64_t timeMarginUsec);
    virtual ~BCTDevice() = default;
    /**
     * Called when the DIM service is refreshed by the server
     * @param dataSet New list of data
     * @param client Client responsible of this callback (can be shared)
     */
    void dataChanged(dim::DIMData::List &dataSet,
                     const ntof::dim::DIMProxyClient &client) override;

    /**
     * Called when an error occured on client (NO-LINK, NOT READY...)
     * @param errMsg Error message
     * @param client Client responsible for this callback (can be shared)
     */
    void errorReceived(const std::string &errMsg,
                       const ntof::dim::DIMProxyClient &client) override;

    /**
     * @brief Waits until a new timing event was received
     * @param[in]  time    timestamp of the occurring event
     * @param[in]  timeoutMsec maximum waiting time in milliseconds (default :
     * 200)
     * @return Dim DataSet
     */
    dim::DIMData::List waitAndGetBCTData(const utils::Timestamp &ts,
                                         int32_t timeoutMsec = 200);

    /**
     * @brief get last received Data
     * @return Dim DataSet
     */
    dim::DIMData::List getLatestBCTData();

private:
    ntof::dim::DIMProxyClient m_client;
    TimedBuffer m_timedBuffer;
};

} /* namespace bct */
} /* namespace ntof */

#endif /* BCTDEVICE_H_ */
