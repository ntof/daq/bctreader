/*
 * BCTReader.cpp
 *
 *  Created on: May 13, 2015
 *      Author: mdonze
 */

#include "BCTReader.h"

#include <chrono>
#include <thread>

#include <easylogging++.h>

#define BCT_TIME_MARGIN 600 * 1000 // 600ms
#define BCT_TIMEOUT 1000

namespace ntof {
namespace bct {

BCTReader::BCTReader(const std::string &svcName,
                     const std::string &eventService,
                     const std::string &bctService) :
    service_(svcName), event_(eventService), bct_(bctService, BCT_TIME_MARGIN)
{}

void BCTReader::timingSync()
{
    dim::EventReader::Data data;
    LOG(INFO) << "Waiting for event...";
    event_.waitForNewEvent(data);
    utils::Timestamp cycleStamp = utils::Timestamp(data.cycleStamp);
    ntof::dim::DIMData::List d;
    if (data.evtType == "CALIBRATION")
    {
        LOG(INFO) << "Calibration mode...";
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        d.emplace_back(AQCSTAMP, "aqcStamp", "", int64_t(data.timeStamp));
        d.emplace_back(CYCLESTAMP, "cycleStamp", "", int64_t(data.cycleStamp));
        d.emplace_back(TOTAL_INTENSITY_PREFERRED, "totalIntensitySingle", "",
                       float(0));
    }
    else
    {
        LOG(INFO) << "Waiting for bct cycleStamp:" << cycleStamp;
        d = bct_.waitAndGetBCTData(cycleStamp, BCT_TIMEOUT);
    }
    // Insert into acquisition now
    pugi::xml_document doc;
    pugi::xml_node dataNode = doc.append_child("dataset");
    for (auto &it : d)
    {
        it.insertIntoAqn(dataNode);
    }
    service_.setData(doc);
}

void BCTReader::run()
{
    while (true)
    {
        timingSync();
    }
}

} /* namespace bct */
} /* namespace ntof */
