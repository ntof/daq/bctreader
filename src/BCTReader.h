/*
 * BCTReader.h
 *
 *  Created on: May 13, 2015
 *      Author: mdonze
 */

#ifndef BCTREADER_H_
#define BCTREADER_H_

#include <DIMXMLService.h>
#include <EventReader.h>

#include "BCTDevice.h"

namespace ntof {
namespace bct {

/**
 * This class reads BCT into a synchronous way
 */
class BCTReader
{
public:
    /*
        indexes for the output service, used in calibration mode
    */
    enum Params
    {
        AQCSTAMP = 0,
        CYCLESTAMP = 1,
        TOTAL_INTENSITY_PREFERRED = 13
    };

    BCTReader(const std::string &svcName,
              const std::string &eventService,
              const std::string &bctService);
    virtual ~BCTReader() = default;
    void timingSync();
    void run();

private:
    ntof::dim::DIMXMLService service_;
    ntof::dim::EventReader event_;
    BCTDevice bct_;
};

} /* namespace bct */
} /* namespace ntof */

#endif /* BCTREADER_H_ */
