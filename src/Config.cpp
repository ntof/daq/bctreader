//
// Created by matteof on 2020-06-15.
//

#include "Config.h"

#include <iostream>

#include <NTOFException.h>
#include <easylogging++.h>

#include <Singleton.hxx>

using namespace ntof::bct;

inline void NTOF_THROW(const std::string &msg)
{
    LOG(ERROR) << msg;
    throw ntof::NTOFException(msg, __FILE__, __LINE__);
}

template class ntof::utils::Singleton<Config>;

const std::string Config::configFile = "/etc/ntof/bctreader.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        NTOF_THROW("Unable to open and parse configuration file! Path: " + file);
    }

    // Parse the config file
    const pugi::xml_node &root = doc.first_child();

    // source
    const pugi::xml_node &source = root.child("source");

    const pugi::xml_attribute &sourceName = source.attribute("name");
    if (sourceName.empty())
    {
        NTOF_THROW("Source name cannot be empty!");
    }
    m_source = sourceName.as_string();

    const pugi::xml_attribute &sourceCycleStampIndex = source.attribute(
        "cyclestamp-index");
    m_cyclestamp_index = sourceCycleStampIndex.as_int(1); // Default 1

    // event
    const pugi::xml_node &event = root.child("event");

    const pugi::xml_attribute &eventName = event.attribute("name");
    if (eventName.empty())
    {
        NTOF_THROW("Event name cannot be empty!");
    }
    m_event = eventName.as_string();

    // destination
    const pugi::xml_node &destination = root.child("destination");

    const pugi::xml_attribute &destinationName = destination.attribute("name");
    if (destinationName.empty())
    {
        NTOF_THROW("Destination name cannot be empty!");
    }
    m_destination = destinationName.as_string();

    // server
    const pugi::xml_node &server = root.child("server");

    const pugi::xml_attribute &serverName = server.attribute("name");
    if (serverName.empty())
    {
        NTOF_THROW("Server name cannot be empty!");
    }
    m_server = serverName.as_string();
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getSource() const
{
    return m_source;
}

uint32_t Config::getCycleStampIndex() const
{
    return m_cyclestamp_index;
}

const std::string &Config::getEvent() const
{
    return m_event;
}

const std::string &Config::getDestination() const
{
    return m_destination;
}

const std::string &Config::getServer() const
{
    return m_server;
}
