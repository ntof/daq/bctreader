//
// Created by matteof on 2020-06-15.
//

#include "test_helpers_bct.hpp"

#include <chrono>

#include <DIMDataSetClient.h>
#include <easylogging++.h>

#include "test_helpers.hpp"

using ntof::dim::DIMData;
using ntof::dim::DIMDataSetClient;
using namespace std::chrono;

DataSetServiceWaiter::DataSetServiceWaiter(const std::string &svcName) :
    m_svcName(svcName)
{}

bool DataSetServiceWaiter::wait(std::function<bool(DIMData::List &)> check,
                                unsigned int ms)
{
    DIMDataSetClient client(m_svcName);
    SignalWaiter waiter;

    waiter.listen(client.dataSignal);

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = client.getLatestData();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}
