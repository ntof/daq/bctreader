//
// Created by matteof on 2020-06-15.
//

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.h"
#include "NTOFException.h"
#include "local-config.h"

namespace bfs = boost::filesystem;
using namespace ntof::bct;
using namespace ntof;

class TestConfig : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConfig);
    CPPUNIT_TEST(test_Config_Exceptions);
    CPPUNIT_TEST(test_FullConfig);
    CPPUNIT_TEST_SUITE_END();

protected:
    bfs::path dataDir_;

public:
    void setUp() { dataDir_ = bfs::path(SRCDIR) / "tests" / "data"; }

    void tearDown()
    {
        Config::load((dataDir_ / "bctreader.xml").string(),
                     (dataDir_ / "misc.xml").string());
    }

    void test_Config_Exceptions()
    {
        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "no_existing_file.xml").string(),
                         (dataDir_ / "misc.xml").string()),
            NTOFException);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "bctreader_no_source.xml").string(),
                         (dataDir_ / "misc.xml").string()),
            NTOFException);
    }

    void test_FullConfig()
    {
        Config::load((dataDir_ / "bctreader.xml").string(),
                     (dataDir_ / "misc.xml").string());
        Config &conf = Config::instance();
        CPPUNIT_ASSERT_EQUAL(std::string("FTN.BCT468.TOF_FESA"),
                             conf.getSource());
        CPPUNIT_ASSERT_EQUAL((uint32_t) 1, conf.getCycleStampIndex());
        CPPUNIT_ASSERT_EQUAL(std::string("Timing/event"), conf.getEvent());
        CPPUNIT_ASSERT_EQUAL(std::string("FTN.BCT468.TOF"),
                             conf.getDestination());
        CPPUNIT_ASSERT_EQUAL(std::string("TOF_BCT_468"), conf.getServer());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfig);
