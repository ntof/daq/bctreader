//
// Created by matteof on 2020-06-15.
//

#ifndef TEST_HELPERS_BCT_HPP__
#define TEST_HELPERS_BCT_HPP__

#include <functional>

#include <DIMData.h>

class DataSetServiceWaiter
{
public:
    explicit DataSetServiceWaiter(const std::string &svcName);
    bool wait(std::function<bool(ntof::dim::DIMData::List &)> check,
              unsigned int ms = 1000);

protected:
    std::string m_svcName;
};

#endif
