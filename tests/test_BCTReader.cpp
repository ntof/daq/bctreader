/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-21T11:38:12+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <exception>
#include <memory>
#include <thread>

#include <DIMXMLService.h>
#include <Timestamp.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "BCTReader.h"
#include "Config.h"
#include "test_helpers.hpp"
#include "test_helpers_bct.hpp"
#include "test_helpers_dim.hpp"

using namespace ntof::bct;
using namespace ntof::dim;

class TestBCTReader : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBCTReader);
    CPPUNIT_TEST(handle_sync);
    CPPUNIT_TEST(handle_calibration);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override { m_dim.reset(new DimTestHelper()); }

    void tearDown() override { m_dim.reset(); }

    void sendTimingEvent(DIMXMLService &srv,
                         const ntof::utils::Timestamp &ts,
                         const std::string &evtType = "noCalib")
    {
        pugi::xml_document doc;
        pugi::xml_node node = doc.append_child("timing");
        pugi::xml_node event = node.append_child("event");
        event.append_attribute("name").set_value(evtType.c_str());
        event.append_attribute("timestamp").set_value(ts);
        event.append_attribute("cyclestamp").set_value(ts);
        event.append_attribute("periodNB").set_value(1000);
        event.append_attribute("dest").set_value("dest");
        event.append_attribute("dest2").set_value("dest2");
        event.append_attribute("user").set_value("user");
        srv.setData(doc);
    }

    void sendBCTEvent(DIMXMLService &srv, const ntof::utils::Timestamp &ts)
    {
        pugi::xml_document doc;
        pugi::xml_node dataNode = doc.append_child("dataset");
        ntof::dim::DIMData acqStampData(0, "aqcStamp", "", ts);
        acqStampData.insertIntoAqn(dataNode);
        ntof::dim::DIMData cycleStampData(1, "cycleStamp", "", ts);
        cycleStampData.insertIntoAqn(dataNode);
        ntof::dim::DIMData cycleNameData(2, "cycleName", "", "NTOF");
        cycleNameData.insertIntoAqn(dataNode);
        srv.setData(doc);
    }

    static void runBCTReader(BCTReader *reader) { reader->timingSync(); }

    void handle_sync()
    {
        Config &conf = Config::instance();
        BCTReader reader(conf.getDestination(), conf.getEvent(),
                         conf.getSource());
        DIMXMLService srvTiming("Timing/event");
        DIMXMLService srvBCT("FTN.BCT468.TOF_FESA");
        ntof::utils::Timestamp ts1 = ntof::utils::Timestamp();

        // This will check we have an update
        DataSetServiceWaiter waiter("FTN.BCT468.TOF");
        {
            // Start Thread with reader
            FutureGuard<void> guard(
                std::async(std::launch::async, runBCTReader, &reader));

            // Send events.
            sendBCTEvent(srvBCT, ts1);
            sendTimingEvent(srvTiming, ts1);

            EQ(true,
               waiter.wait(
                   [&ts1](DIMData::List &data) {
                       DIMData cycleStamp = getData("cycleStamp",
                                                    DIMData(0, data));
                       return cycleStamp.getLongValue() == ts1.getValue();
                   },
                   1000));

            EQ(true,
               waiter.wait(
                   [&ts1](DIMData::List &data) {
                       DIMData timeStamp = getData("aqcStamp", DIMData(0, data));
                       return timeStamp.getLongValue() == ts1.getValue();
                   },
                   1000));
        }

        {
            // Event ooo old
            FutureGuard<void> guard(
                std::async(std::launch::async, runBCTReader, &reader));

            std::this_thread::sleep_for(std::chrono::milliseconds(100));

            // Send events.
            ntof::utils::Timestamp ts2 = ntof::utils::Timestamp(ts1.getValue() +
                                                                601 * 1000);
            sendBCTEvent(srvBCT, ts2);
            sendTimingEvent(
                srvTiming, ntof::utils::Timestamp(ts2.getValue() - 601 * 1000));

            EQ(false,
               waiter.wait(
                   [&ts2](DIMData::List &data) {
                       DIMData cycleStamp = getData("cycleStamp",
                                                    DIMData(0, data));
                       return cycleStamp.getLongValue() == ts2.getValue();
                   },
                   1000));
        }

        {
            // Event out of margin
            FutureGuard<void> guard(
                std::async(std::launch::async, runBCTReader, &reader));
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

            // Send events.
            ntof::utils::Timestamp ts3 = ntof::utils::Timestamp(
                ts1.getValue() + 601 * 1000 + 2202 * 1000);
            sendBCTEvent(srvBCT, ts3);
            sendTimingEvent(
                srvTiming, ntof::utils::Timestamp(ts3.getValue() - 601 * 1000));

            EQ(false,
               waiter.wait(
                   [&ts3](DIMData::List &data) {
                       DIMData cycleStamp = getData("cycleStamp",
                                                    DIMData(0, data));
                       return cycleStamp.getLongValue() == ts3.getValue();
                   },
                   1000));
        }
    }

    void handle_calibration()
    {
        Config &conf = Config::instance();
        BCTReader reader(conf.getDestination(), conf.getEvent(),
                         conf.getSource());
        DIMXMLService srvTiming("Timing/event");
        // calibration doesn't need BCT

        // This will check we have an update
        DataSetServiceWaiter waiter("FTN.BCT468.TOF");

        // Start Thread with reader
        FutureGuard<void> guard(
            std::async(std::launch::async, runBCTReader, &reader));

        // Send events.
        ntof::utils::Timestamp ts1 = ntof::utils::Timestamp();
        sendTimingEvent(srvTiming, ts1, "CALIBRATION");

        EQ(true,
           waiter.wait(
               [&ts1](DIMData::List &data) {
                   DIMData cycleStamp = getData("cycleStamp", DIMData(0, data));
                   return cycleStamp.getLongValue() == ts1.getValue();
               },
               1000));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestBCTReader);
